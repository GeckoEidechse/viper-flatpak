# Viper is now on [Flathub](https://flathub.org/apps/details/com.github._0negal.Viper). Check the corresponding repository [here](https://github.com/flathub/com.github._0negal.Viper/)!

# Viper-flatpak

Proof-of-concept for running [Viper](https://github.com/0neGal/viper/) in a Flatpak environment.

## Building

Install Flatpak electron dependency

```
flatpak install flathub org.electronjs.Electron2.BaseApp//20.08
```

Simple build

```
flatpak-builder build-dir/ com.github.onegal.Viper.yaml
```

Build and run:

```
flatpak-builder --user --install --force-clean build-dir/ com.github.onegal.Viper.yaml
flatpak run com.github.onegal.Viper
```

## Updating manifest

Use the provided Python script to auto-update the manifest to the newest version of Viper

```
python3 update-version.py
```